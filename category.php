<?php
/**
 * Template for displaying Category Archive pages
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
			<div id="content" role="main" class="category-list">

				<h1 class="page-title"><?php echo single_cat_title( '', false ) ?></h1>
				<?php if ($category_description = category_description()): ?>
					<div class="archive-meta">
						<?php echo $category_description ?>
					</div>
				<?php endif; ?>
				<?php
				/*
				 * Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				get_template_part( 'loop', 'category' );
				?>

			</div><!-- #content -->
		</div><!-- #container -->
<?php get_footer(); ?>
