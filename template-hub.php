
<?php
/**
Template Name: Концентратор. Шаблон для страницы "Портфолио"
 */
get_header();
?>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/bower_components/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/bower_components/masonry/dist/masonry.pkgd.js"></script>
<div class="post">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div>
            <h1><?php the_title(); ?></h1>
            <?php
                $postId = get_the_ID();
                $post = get_post($postId);
                $content = strip_shortcodes($post->post_content);
                $myQuery = new WP_Query();
                $allPages = $myQuery->query(['post_type' => 'page', 'post_status' => 'publish', 'posts_per_page' => -1]);
                $children = get_page_children($post->ID, $allPages);
            ?>
            <?php echo $content ?>
            <div class="entry-content masonry-gallery">
                <div class="grid grid-5">
                    <div class="grid-sizer"></div>
                    <div class="gutter-sizer"></div>
                    <?php foreach($children as $i => $child): ?>
                        <?php $smallImage = wp_get_attachment_image_src(get_post_thumbnail_id($child->ID), 'medium'); ?>
                        <a href="<?php echo get_permalink($child->ID) ?>" title="<?php echo esc_attr($child->post_title) ?>">
                            <div class="grid-item">
                                <div class="wrapper">
                                    <div class="title">
                                        <?php echo esc_attr($child->post_title) ?>
                                    </div>
                                </div>
                                <div class="image scaled-image" style="background-image: url('<?php echo $smallImage[0] ?>')">
                                    <div class="aspect-ratio-content"></div>
                                </div>
                            </div>
                        </a>
                        <?php if ($i == 5): ?>
                            <a href="/" title="Перейти на главную">
                                <div class="grid-item grid-item-width-2">
                                    <div class="wrapper">
                                        <div class="title">
                                            Concept Art & Design
                                        </div>
                                    </div>
                                    <div class="image scaled-image image-width-2" style="background-image: url('<?php bloginfo( 'stylesheet_directory' ) ?>/images/logo-wide.png')">
                                        <div class="aspect-ratio-content"></div>
                                    </div>
                                </div>
                            </a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="toolbar right"><a class="link link-red link-big" href="/контакты/">Заказать</a></div>
        </div><!-- #post-## -->
    <?php endwhile; // end of the loop. ?>
</div>
<script type="text/javascript">
    $(function(){
        var $grid = $('.grid').masonry({
            itemSelector: '.grid-item',
            percentPosition: true,
            columnWidth: '.grid-sizer',
            gutter: '.gutter-sizer'
        });
        // layout Masonry after each image loads
        $grid.imagesLoaded().progress( function() {
            $grid.masonry('layout');
        });
    });
</script>
<?php get_footer();
