<?php
/**
 * @var WP_Post[] $attachments Images of gallery
 */
?>
<div class="cad-gallery fotorama-js" data-width="100%" data-nav="thumbs" data-thumbheight="107" data-allowfullscreen="true" data-hash="true" data-ratio="3/2" data-autoplay="true" data-loop="true">
    <?php foreach($attachments as $i => $image): ?>
        <?php
            $smallImage = wp_get_attachment_image_src($image->ID, 'medium', false);
            $fullImage = wp_get_attachment_image_src($image->ID, 'full', false);
        ?>
        <a href="<?php echo $fullImage[0] ?>">
            <img src="<?php echo  $smallImage[0] ?>" width="150" height="107" data-post-content="<?php echo $image->post_content ?>" data-post-title="<?php echo $image->post_title ?>" />
            <div class="wrapper">
                <div class="title">
                    <?php echo $image->post_title ?>
                </div>
                <div class="text">
                    <?php echo $image->post_content ?>
                </div>
            </div>
        </a>
    <?php endforeach; ?>
</div>
<script type="text/javascript">
    if (typeof noPhotoRamaJs == 'undefined' ||!noPhotoRamaJs){
        $(function(){
            $('.fotorama-js').fotorama();
        });
    }
</script>