<?php
/**
Template Name: Для страницы "О студии"
*/
get_header();
?>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/bower_components/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/bower_components/masonry/dist/masonry.pkgd.js"></script>
<div class="container post">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <?php
        $postId = get_the_ID();
        $post = get_post($postId);
        $content = strip_shortcodes($post->post_content);
        $args = array('post_type' => 'attachment', 'numberposts' => 1, 'post_status' => null, 'post_parent' => $postId);
        $attachments = get_posts($args);
        $image = isset($attachments[0]) ? $attachments[0] : false;
        if ($image){
            $metadata = wp_get_attachment_metadata($image->ID);
        }
        ?>
        <?php if ($image): ?>
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div style="margin-right: 20px;">
                        <img src="/wp-content/uploads/<?php echo $metadata['file'] ?>" />
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <?php
                $myQuery = new WP_Query();
                $allPages = $myQuery->query(['post_type' => 'page', 'post_status' => 'publish', 'posts_per_page' => -1]);
                $children = get_page_children(77, $allPages);
            ?>
<!--            <pre>--><?php //print_r($children) ?><!--</pre>-->
            <div class="col-md-12">
                <div class="entry-content masonry-gallery">
                    <div class="grid grid-5">
                        <div class="grid-sizer"></div>
                        <div class="gutter-sizer"></div>
                        <?php foreach($children as $i => $child): ?>
                            <?php $smallImage = wp_get_attachment_image_src(get_post_thumbnail_id($child->ID), 'medium'); ?>
                            <a href="<?php echo get_permalink($child->ID) ?>" title="<?php echo esc_attr($child->post_title) ?>">
                                <div class="grid-item">
                                    <div class="wrapper">
                                        <div class="title">
                                            <?php echo esc_attr($child->post_title) ?>
                                        </div>
                                    </div>
                                    <div class="image scaled-image" style="background-image: url('<?php echo $smallImage[0] ?>')">
                                        <div class="aspect-ratio-content"></div>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function(){
                        var $grid = $('.grid').masonry({
                            itemSelector: '.grid-item',
                            percentPosition: true,
                            columnWidth: '.grid-sizer',
                            gutter: '.gutter-sizer'
                        });
                        // layout Masonry after each image loads
                        $grid.imagesLoaded().progress( function() {
                            $grid.masonry('layout');
                        });
                    });
                </script>
            </div>
        </div>
</div><!-- #post-## -->
    <?php endwhile; // end of the loop. ?>
<?php get_footer();

