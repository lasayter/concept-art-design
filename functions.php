<?php
/**
 * Setup Concept Art & Design theme
 */

function cad_render_file($_viewFile_, $_data_=null, $_return_=false){
// we use special variable names here to avoid conflict when extracting data
    if(is_array($_data_)){
        extract($_data_,EXTR_PREFIX_SAME, 'data');
    } else {
        $data = $_data_;
    }

    if($_return_){
        ob_start();
        ob_implicit_flush(false);
        require($_viewFile_);
        return ob_get_clean();
    } else {
        require($_viewFile_);
    }
}

/**
 * Class Image
 */
class Image {

    public $src = '';
    public $alt = '';
    public $title = '';
    public $class = '';
    public $id = '';

    public static function attributes(){
        return ['src', 'alt', 'title', 'class', 'id'];
    }

    /**
     * @return string
     */
    public function __toString(){
        $doc = new DOMDocument();
        $element = $doc->createElement('img');
        $attributes = static::attributes();
        foreach($attributes as $attribute){
            $element->setAttribute($attribute, $this->{$attribute});
        }
        $doc->appendChild($element);
        return $doc->saveHTML();
    }
}

add_action( 'init', 'concept_art_and_design_init' );

function concept_art_and_design_init() {
}


/**
 * Add the admin options page
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'options.php');
add_action('admin_menu', 'plugin_admin_add_page');

function plugin_admin_add_page() {
    add_options_page('Корпоративные настройки', 'Корпоративные настройки', 'manage_options', 'concept_art_and_design', 'concept_art_and_design_page');
}

add_action('admin_init', 'plugin_admin_init');
function plugin_admin_init(){
    register_setting( 'concept_art_and_design_options', 'concept_art_and_design_options', 'concept_art_and_design_options_validate' );
    add_settings_section('concept_art_and_design_main', 'Основные настройки', 'concept_art_and_design_section_text', 'concept_art_and_design');
    add_settings_field('concept_art_and_design_text_string', 'Номер телефона', 'concept_art_and_design_setting_string', 'concept_art_and_design', 'concept_art_and_design_main');
    add_settings_field('concept_art_and_design_email_string', 'E-mail', 'concept_art_and_design_email_string', 'concept_art_and_design', 'concept_art_and_design_main');
}

// Add Thumbnail Support
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    add_image_size('full-screen', 1800, 1200, false);
}


/**
 * Switch theme hook initialization
 */
function concept_art_and_design__rewrite_flush() {
    concept_art_and_design_init();
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'concept_art_and_design__rewrite_flush' );

/**********************************************************************************************************************
 * Helpers for content management
 **********************************************************************************************************************/

/**
 * Return CSS classes for gallery HTML block for text content extraction.
 * @return array
 */
function cad_get_galleries_classes(){
    return ['gallery'];
}

/**
 * @param string $html
 * @return Image[]
 */
function cad_extract_images($html){
    $doc = new DOMDocument();
    @$doc->loadHTML($html);

    $tags = $doc->getElementsByTagName('img');
    $attributes = Image::attributes();

    $images = [];
    foreach ($tags as $tag) {
        $image = new Image();
        foreach($attributes as $attribute){
            $image->{$attribute} = $tag->getAttribute($attribute);
        }
        $image->src = str_replace('-150x150', '', $image->src);
        $images[] = $image;
    }
    return $images;
}

function cad_remove_images($html, $galleriesClasses = null){
    if (is_null($galleriesClasses)){
        $galleriesClasses = cad_get_galleries_classes();
    }
    $doc = new DOMDocument();
    @$doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
    $selector = new DOMXPath($doc);
    foreach($galleriesClasses as $galleriesClass){
        foreach($selector->query('//div[contains(attribute::class, "' . $galleriesClass . '")]') as $e ) {
            $e->parentNode->removeChild($e);
        }
    }
    return $doc->saveHTML($doc->documentElement);
}

function cad_remove_wp_images($html){
    $doc = new DOMDocument();
    @$doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
    $selector = new DOMXPath($doc);
    foreach($selector->query('//a/img') as $e ) {
        $e->parentNode->removeChild($e);
    }
    return $doc->saveHTML($doc->documentElement);
}

function cad_get_image_url($postObject){
    $url = $postObject->guid;
    $parts = explode("/", $url);
    $parts[count($parts) - 1] = $postObject->metadata['sizes']['medium']['file'];
    return implode("/", $parts);
}

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'photoswipe.php');


add_action( 'after_setup_theme', 'override_default_gallery' );

function override_default_gallery() {
    // Override gallery shortcode
    remove_shortcode('gallery', 'gallery_shortcode');
    add_shortcode('gallery', 'cad_gallery_shortcode');
}
function cad_gallery_shortcode($attr) {
    $post = get_post();
    static $instance = 0;
    $instance++;

    if ( ! empty( $attr['ids'] ) ) {
        // 'ids' is explicitly ordered, unless you specify otherwise.
        if ( empty( $attr['orderby'] ) ) {
            $attr['orderby'] = 'post__in';
        }
        $attr['include'] = $attr['ids'];
    }

    /**
     * Filter the default gallery shortcode output.
     *
     * If the filtered output isn't empty, it will be used instead of generating
     * the default gallery template.
     *
     * @since 2.5.0
     * @since 4.2.0 The `$instance` parameter was added.
     *
     * @see gallery_shortcode()
     *
     * @param string $output   The gallery output. Default empty.
     * @param array  $attr     Attributes of the gallery shortcode.
     * @param int    $instance Unique numeric ID of this gallery shortcode instance.
     */
    $output = apply_filters( 'post_gallery', '', $attr, $instance );
    if ( $output != '' ) {
        return $output;
    }

    $html5 = current_theme_supports( 'html5', 'gallery' );
    $atts = shortcode_atts( array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post ? $post->ID : 0,
        'itemtag'    => $html5 ? 'figure'     : 'dl',
        'icontag'    => $html5 ? 'div'        : 'dt',
        'captiontag' => $html5 ? 'figcaption' : 'dd',
        'columns'    => 3,
        'size'       => 'thumbnail',
        'include'    => '',
        'exclude'    => '',
        'link'       => ''
    ), $attr, 'gallery' );

    $id = intval( $atts['id'] );

    if ( ! empty( $atts['include'] ) ) {
        $_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
        $attachments = array();
        foreach ( $_attachments as $key => $val ) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif ( ! empty( $atts['exclude'] ) ) {
        $attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
    } else {
        $attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
    }

    if ( empty( $attachments ) ) {
        return '';
    }

    $output = cad_render_file(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'gallery.php', [
        'attachments' => $attachments,
    ], true);

//    foreach ( $attachments as $id => $attachment ) {
//
//        $attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
//        if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
//            $image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
//        } elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
//            $image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
//        } else {
//            $image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
//        }
//        $image_meta  = wp_get_attachment_metadata( $id );
//
//        $orientation = '';
//        if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
//            $orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
//        }
//        $output .= "<{$itemtag} class='gallery-item'>";
//        $output .= "
//			<{$icontag} class='gallery-icon {$orientation}'>
//				$image_output
//			</{$icontag}>";
//        if ( $captiontag && trim($attachment->post_excerpt) ) {
//            $output .= "
//				<{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
//				" . wptexturize($attachment->post_excerpt) . "
//				</{$captiontag}>";
//        }
//        $output .= "</{$itemtag}>";
//        if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
//            $output .= '<br style="clear: both" />';
//        }
//    }
    return $output;
}