<?php
/**
Template Name: ??? ???????? ??????????? ?????.
 */
?>

<?php get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">
                <h1><?php the_title(); ?></h1>
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div><!-- .entry-content -->
                    </div>
                <?php endwhile; // end of the loop. ?>
            </div>
            <div class="col-md-3 col-md-pull-9">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div><!-- #post-## -->
<?php get_footer();