<?php

    $post = $valid = true;
    $fields = ['clientName', 'email', 'message'];
    foreach($fields as $field){
        $$field = '';
    }

    $errors = [];
    if (!empty($_POST)){
        foreach($fields as $field){
            $$field = isset($_POST[$field]) ? $_POST[$field] :  null;
            if (!$$field){
                $valid = false;
                $errors[$field] = 'Обязательно для заполнения';
            }
        }
        if ($valid){
            $date = date('Y-m-d H:i:s');
            $fullMessage = "
            Дата: $date
            Имя: $clientName
            E-mail или телефон: $email
            Сообщение: $message
            ";

            $options = get_option('concept_art_and_design_options');
            $email = $options['concept_art_and_design_email'];

            if ($email){
                $result = mail($email, 'Новый отзыв на gs-concept.ru', $fullMessage);
                if ($result){
                    ?>
                    <div class="center">
                        Спасибо! Вы скоро получите ответ.
                    </div>
                    <?php
                }
            }
        }
    } else {
        $post = false;
    }
?>
<?php if (!$post || !$valid): ?>
    <form action="" method="post">
        <div class="form-group <?php echo $errors['clientName'] ? 'has-error' : '' ?>">
            <input type="text" class="form-control" id="clientName" placeholder="Имя" name="clientName" value="<?php echo $clientName ?>">
            <?php if ($errors['clientName']): ?>
                <div class="small center">
                    <?php echo $errors['clientName'] ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo $errors['email'] ? 'has-error' : '' ?>">
            <input type="text" class="form-control" id="email" placeholder="E-mail или телефон" name="email" value="<?php echo $email ?>">
            <?php if ($errors['email']): ?>
                <div class="small center">
                    <?php echo $errors['email'] ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="form-group <?php echo $errors['message'] ? 'has-error' : '' ?>">
            <textarea class="form-control" id="message" placeholder="Сообщение" name="message"><?php echo $message ?></textarea>
            <?php if ($errors['message']): ?>
                <div class="small center">
                    <?php echo $errors['message'] ?>
                </div>
            <?php endif; ?>
        </div>
        <button type="submit" class="link link-red">Заказать</button>
    </form>
<?php endif; ?>