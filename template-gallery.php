<?php
/**
Old Template Name: Simple Masonry Gallery
*/
get_header();

?>

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/bower_components/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/bower_components/masonry/dist/masonry.pkgd.js"></script>
<div class="container">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content masonry-gallery">
            <div class="grid grid-3">
                <div class="grid-sizer"></div>
                <?php
                    ob_start();
                    the_content();
                    $html = ob_get_clean();
                    $images = cad_extract_images($html);
                ?>
                <?php foreach($images as $image): ?>
                    <div class="grid-item">
                        <div class="wrapper">
                            <div class="title">
                                <?php echo $image->alt ?>
                            </div>
                        </div>
                        <?php /** @var Image $image */ ?>
                        <?php echo $image ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div><!-- .entry-content -->
        <script type="text/javascript">
            $(function(){
                var $grid = $('.grid').masonry({
                    itemSelector: '.grid-item',
                    percentPosition: true,
                    columnWidth: '.grid-sizer'
                });
                // layout Masonry after each image loads
                $grid.imagesLoaded().progress( function() {
                    $grid.masonry('layout');
                });
            });
        </script>
    </div><!-- #post-## -->
    <?php endwhile; // end of the loop. ?>
</div>
<?php get_footer();
