
<?php
/**
Template Name: Clear Html
*/
get_header();
?>

<div class="container">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div class="post">
            <?php the_content(); ?>
        </div>
    </div><!-- #post-## -->
    <?php endwhile; // end of the loop. ?>
<?php get_footer();
