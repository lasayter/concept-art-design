
<?php
/**
Template Name: Страница проекта
*/
get_header();
$maxImageWidth = 1800;

?>

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/bower_components/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/bower_components/masonry/dist/masonry.pkgd.js"></script>
<div class="container">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <h1 class="entry-title"><?php the_title(); ?></h1>
            <?php
                $postId = get_the_ID();
                $post = get_post($postId);
                $content = cad_remove_wp_images(strip_shortcodes($post->post_content));
                $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => get_the_ID() );
                $attachments = get_posts($args);
                if ($attachments) {
                    foreach ($attachments as $i => $attachment) {
                        $metadata = wp_get_attachment_metadata($attachment->ID);
                        $attachment->metadata = $metadata;
                    }
                }
            ?>
            <?php echo $content ?>
            <div class="entry-content masonry-gallery">
                <div class="grid grid-5">
                    <div class="grid-sizer"></div>
                    <div class="gutter-sizer"></div>
                    <?php foreach($attachments as $i => $image): ?>
                        <?php
                            $smallImage = wp_get_attachment_image_src($image->ID, 'full', false);
                        ?>
                        <div class="grid-item">
                            <div class="wrapper">
                                <div class="title">
                                    <?php echo $image->title ?>
                                </div>
                            </div>
                            <div class="photoswipe-image image scaled-image"
                                 style="background-image: url('<?php echo cad_get_image_url($image) ?>')"
                                 data-id="<?php echo $i ?>"
                                 data-width="<?php echo $image->metadata['width'] ?>"
                                 data-height="<?php echo $image->metadata['height'] ?>"
                                 data-src="<?php echo $smallImage[0] ?>
                                 ">
                                <div class="aspect-ratio-content"></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div><!-- .entry-content -->

        <script type="text/javascript">
            $(function(){
                var $grid = $('.grid').masonry({
                    itemSelector: '.grid-item',
                    percentPosition: true,
                    columnWidth: '.grid-sizer',
                    gutter: '.gutter-sizer'
                });
                // layout Masonry after each image loads
                $grid.imagesLoaded().progress( function() {
                    $grid.masonry('layout');
                });
            });
        </script>
        <?php
            cad_init_photoswipe_gallery();
        ?>
    </div><!-- #post-## -->
    <?php endwhile; // end of the loop. ?>
</div>
<?php get_footer();
