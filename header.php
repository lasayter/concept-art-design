<?php
/**
 * Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">.
 *
 * @package WordPress
 * @subpackage concept-art-design
 * @since Concept Art Design 0.1.0
 */

$options = $options = get_option('concept_art_and_design_options');
$phoneNumber = $options['concept_art_and_design_phone_number'];

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />

        <title><?php
            /*
             * Print the <title> tag based on what is being viewed.
             */
            global $page, $paged;

            wp_title( '|', true, 'right' );

            // Add the blog name.
            bloginfo( 'name' );

            // Add the blog description for the home/front page.
            $site_description = get_bloginfo( 'description', 'display' );
            if ( $site_description && ( is_home() || is_front_page() ) )
                echo " | $site_description";

            // Add a page number if necessary:
            if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
                echo esc_html( ' | ' . sprintf( __( 'Page %s', 'Concept Art & Design' ), max( $paged, $page ) ) );

            ?></title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap core CSS -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,200,100,700,700italic,400italic,100italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?_<?php echo time() ?>" />
        <link rel="stylesheet" type="text/css" media="screen" href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php
        /*
         * We add some JavaScript to pages with the comment form
         * to support sites with threaded comments (when in use).
         */
        if ( is_singular() && get_option( 'thread_comments' ) )
            wp_enqueue_script( 'comment-reply' );

        /*
         * Always have wp_head() just before the closing </head>
         * tag of your theme, or you will break many plugins, which
         * generally use this hook to add elements to <head> such
         * as styles, scripts, and meta tags.
         */
        wp_head();
        ?>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src='<?php bloginfo( 'stylesheet_directory' ) ?>/bower_components/jquery/dist/jquery.min.js'></script>
        <script src='<?php bloginfo( 'stylesheet_directory' ) ?>/js/bootstrap.min.js'></script>
    </head>
    <body <?php body_class(); ?> id="cad">

        <!-- Fixed navbar -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">????????? ?? ?????</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Concept Art & Design</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav pull-left hidden-xs hidden-sm">
                        <li><a href="/" title="Concept Art & Design"><img src="<?php bloginfo('stylesheet_directory') ?>/images/logo_ru.png"></a></li>
                    </ul>
                    <?php wp_nav_menu([
                        'container' => 'ul',
                        'menu_class' => 'nav navbar-nav navbar-center cad-main-nav',
                        'theme_location' => 'primary',
                    ]); ?>
                    <ul class="nav navbar-nav navbar-right pull-right">
                        <li class="menu-phone-container">
                            <a href="tel:<?php echo $phoneNumber ?>"><div class="menu-phone"><nobr><?php echo $phoneNumber ?></nobr></div></a>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        <div class="container" id="wrap">
            <?php /*
            <div class="header clearfix">
                <ul class="nav pull-left">
                    <li><a href="/" title="Concept Art & Design"><img src="<?php bloginfo('stylesheet_directory') ?>/images/logo_161x57.png"></a></li>
                </ul>
                <nav>
                    <?php wp_nav_menu([
                        'container' => 'ul',
                        'menu_class' => 'nav nav-pills cad-main-nav',
                        'theme_location' => 'primary',
                    ]); ?>
                    <ul class="nav pull-right ">
                        <li class="menu-phone-container">
                            <div class="menu-phone"><nobr><?php echo $phoneNumber ?></nobr></div>
                        </li>
                    </ul>
                </nav>
            </div>
            */ ?>