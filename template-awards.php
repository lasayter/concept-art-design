<?php
/**
Template Name: Для страницы "Награды".
 */
get_header();
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min.js"></script>
<script type="text/javascript">
    var noPhotoRamaJs = 1;
</script>
    <div class="container">
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <h1><?php the_title() ?></h1>
                <div class="entry-content awards">
                    <?php the_content(); ?>
                </div><!-- .entry-content -->
            </div>
        <?php endwhile; // end of the loop. ?>
    </div><!-- #post-## -->
    <script type="text/javascript">
        $(function(){
            $('.cad-gallery a').css('display', 'block').css('border', '1px solid #EBEBEB').css('width', '33%');
            $('.cad-gallery a img').css('display', 'block').width('100%').height('auto');
            var $container = $('.cad-gallery');
            $container.imagesLoaded( function(){
                $container.masonry({
                    itemSelector : 'a'
                });
            });
        });
    </script>
<?php get_footer();