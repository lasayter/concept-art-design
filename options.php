<?php

function concept_art_and_design_section_text(){
    echo '';
}

function concept_art_and_design_text_string(){
    echo ('');
}

function concept_art_and_design_setting_string(){
    $options = get_option('concept_art_and_design_options');
    echo "<input id='concept_art_and_design_phone_number' name='concept_art_and_design_options[concept_art_and_design_phone_number]' size='40' type='text' value='{$options['concept_art_and_design_phone_number']}' /><br>";
}

function concept_art_and_design_email_string(){
    $options = get_option('concept_art_and_design_options');
    echo "<input id='concept_art_and_design_email' name='concept_art_and_design_options[concept_art_and_design_email]' size='40' type='text' value='{$options['concept_art_and_design_email']}' />";
}

function concept_art_and_design_options_validate($input) {
    $newInput['concept_art_and_design_phone_number'] = trim($input['concept_art_and_design_phone_number']);
    $newInput['concept_art_and_design_email'] = trim($input['concept_art_and_design_email']);
    return $newInput;
}

function concept_art_and_design_page() {
    ?>
    <div>
        <h2>Корпоративные настройки</h2>
        Различные настройки, добавленные согласно требованиям бизнеса.
        <form action="options.php" method="post">
            <?php settings_fields('concept_art_and_design_options'); ?>
            <?php do_settings_sections('concept_art_and_design'); ?>
            <input name="Submit" type="submit" value="<?php esc_attr_e('Сохранить изменения'); ?>" />
        </form></div>

    <?php
}