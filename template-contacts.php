<?php
/**
Template Name: Страница "Контакты"
 */
get_header();
$options = $options = get_option('concept_art_and_design_options');
$phoneNumber = $options['concept_art_and_design_phone_number'];
$phoneNumberClear = preg_replace("/[^0-9,.]/", "", $phoneNumber);
?>
<div class="container post post-two-columns">
    <div class="row">
        <div class="col-md-3 col-xs-12">
            <div class="block">
                <h1 class="mini center"><?php the_title(); ?></h1>
                <address>
                    Дизайн завод FLACON
                </address>
                <address>
                    Москва, Б. Новодмитровская, д. 36, стр. 12
                </address>
                <a id="email"></a>
            </div>
            <div class="block">
                <div itemscope itemtype="http://schema.org/LocalBusiness">
                    <h1 itemprop="name" class="hidden">Concept Art & Design</h1>
                    <span itemprop="telephone"><a class="phone" href="tel:+<?php echo $phoneNumberClear ?>"><?php echo $phoneNumber ?></a></span>
                </div>
            </div>
            <div style="height: 60px;"></div>
            <div class="block">
                <h1 class="mini center">Обратная связь</h1>
                <?php include('feedback-form.php'); ?>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="google-map">
                <div style="width:100%; max-width:100%; overflow:hidden;height:100%;">
                    <div id="embed-map-display" style="height:100%; width:100%;max-width:100%;">
                        <iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Москва,+Б.+Новодмитровская,+д.+36,+стр.+12&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU">

                        </iframe>
                    </div>
                </div>
                <div class="content">
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#email').prop('href', 'mailto:info@gs-concept.ru').text('info@gs-concept.ru');
    });
</script>
<?php get_footer();
