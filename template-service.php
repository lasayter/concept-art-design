<?php
/**
Template Name: Страница конкретной услуги
*/
get_header();

?>

<div class="container post">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
            <?php
                $postId = get_the_ID();
                $post = get_post($postId);
                $content = strip_shortcodes($post->post_content);
                $args = array('post_type' => 'attachment', 'numberposts' => 1, 'post_status' => null, 'post_parent' => $postId);
                $attachments = get_posts($args);
                $image = isset($attachments[0]) ? $attachments[0] : false;
                if ($image){
                    $metadata = wp_get_attachment_metadata($image->ID);
                }
            ?>
        <?php if ($image): ?>
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div style="margin-right: 20px;">
                        <img src="/wp-content/uploads/<?php echo $metadata['file'] ?>" />
                        <h1 class="h1"><?php the_title(); ?></h1>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <?php echo $content ?>
                </div>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <?php echo $content ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endwhile; // end of the loop. ?>
</div>
<script type="text/javascript">
    $(function(){
        $('td:contains("x")', 'table').html("<div class='checker'></div>");
    });
</script>
<?php get_footer();
