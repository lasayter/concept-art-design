<?php
/**
 * Main template file
 *
 * @package WordPress
 * @subpackage concept-art-design
 */

get_header(); ?>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/jssor.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/jssor.slider.js"></script>
    <script>
        jQuery(document).ready(function ($) {

            var _SlideshowTransitions = [
                //Fade
                { $Duration: 1200, $Opacity: 2 }
            ];

            var _CaptionTransitions = {
                "FADE": { $Duration: 900, $Opacity: 2 },
                "CLIP|LR": { $Duration: 100, $Clip: 3, $Easing: { $Clip: $JssorEasing$.$EaseInOutCubic }, $Opacity: 2 }
            };

            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 3000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                    $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                    $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                    $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                    $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                },
                $CaptionSliderOptions: {                            //[Optional] Options which specifies how to animate caption
                    $Class: $JssorCaptionSlider$,                   //[Required] Class to create instance to animate caption
                    $CaptionTransitions: _CaptionTransitions,       //[Required] An array of caption transitions to play caption, see caption transition section at jssor slideshow transition builder
                    $PlayInMode: 1,                                 //[Optional] 0 None (no play), 1 Chain (goes after main slide), 3 Chain Flatten (goes after main slide and flatten all caption animations), default value is 1
                    $PlayOutMode: 3                                 //[Optional] 0 None (no play), 1 Chain (goes before main slide), 3 Chain Flatten (goes before main slide and flatten all caption animations), default value is 1
                },
                $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 10,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 10,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };




            function smartScaleSlider(){
                var width = $('#row-with-slider').width();
                if (width){
                    jssor_slider1 && jssor_slider1.$ScaleWidth(width);
                } else {
                    window.setTimeout(smartScaleSlider, 30);
                }
            }

            var width = $('#row-with-slider').width();
            $('#slider1_container').width(width);
            $('#slides').width(width);
            if (1){
                var jssor_slider1 = new $JssorSlider$("slider1_container", options);
                $(window).bind("load", smartScaleSlider);
                $(window).bind("resize", smartScaleSlider);
                $(window).bind("orientationchange", smartScaleSlider);
                smartScaleSlider();
            }
            //responsive code end
        });
    </script>

    <div class="row">
        <div class="order-button-container">
            <a href="/контакты/" class="order-button">Заказать</a>
        </div>
    </div>

</div><!--/.container-->
<div class="container-fluid">
    <div class="row" id="row-with-slider">
        <!-- Jssor Slider Begin -->
        <!-- To move inline styles to css file/block, please specify a class name for each element. -->
        <div class='main-slider hidden-xs' id="slider1_container" style="position: relative; top: 0; left: 0; width: 1920px; height: 600px; overflow: hidden; ">

            <!-- Loading Screen -->
<!--            <div u="loading" style="position: absolute; top: 0px; left: 0px;">-->
<!--                <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;-->
<!--                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">-->
<!--                </div>-->
<!--                <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;-->
<!--                top: 0px; left: 0px;width: 100%;height:100%;">-->
<!--                </div>-->
<!--            </div>-->

            <!-- Slides Container -->
            <div id="slides" u="slides" style="cursor: move; position: absolute; left: 0; top: 0; width: 100%; height: 600px; overflow: hidden;">
                <div>
                    <img u="image" src="<?php bloginfo('stylesheet_directory') ?>/images/main-page-slides/header_bg1.jpg" />
                    <div u="caption" t="CLIP|LR" du="1500" class="main-slide-title">
                        Дизайн и ремонт под ключ
                    </div>
                    <div u="caption" t="CLIP|LR" du="100" class="main-slide-subtitle">
                        Рисуем дом в котором хочется жить
                    </div>
                </div>
                <div>
                    <img u="image" src="<?php bloginfo('stylesheet_directory') ?>/images/main-page-slides/header_bg2.jpg" />
                    <div u="caption" t="CLIP|LR" du="1500" class="main-slide-title">
                        Дизайн и ремонт под ключ
                    </div>
                    <div u="caption" t="CLIP|LR" du="100" class="main-slide-subtitle">
                        <strong>260</strong> реализованных проектов
                    </div>
                </div>
                <div>
                    <img u="image" src="<?php bloginfo('stylesheet_directory') ?>/images/main-page-slides/header_bg3.jpg" />
                    <div u="caption" t="CLIP|LR" du="1500" class="main-slide-title">
                        Дизайн и ремонт под ключ
                    </div>
                    <div u="caption" t="CLIP|LR" du="100" class="main-slide-subtitle">
                        <strong>50 000</strong> спланированных квадратных метров
                    </div>
                </div>
            </div>

            <!--#region Bullet Navigator Skin Begin -->
            <!-- Help: http://www.jssor.com/development/slider-with-bullet-navigator-jquery.html -->
            <!-- bullet navigator container -->
            <div u="navigator" class="navigator">
                <!-- bullet navigator item prototype -->
                <div u="prototype"></div>
            </div>
            <!--#endregion Bullet Navigator Skin End -->
        </div>
        <!-- Jssor Slider End -->
    </div>
</div>
<div class="container">

    <div class="row">
        <div class="tiles h-padding-80" id="main-page-tiles">
            <div class="col-md-4 col-sm-4 sol-xs-12">
                <div class="tile tile-yellow tile-about-projects">
                    <div class="title">
                        260
                    </div>
                    <div class="text">
                        <div>
                            Реализованных<br>проектов
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 sol-xs-12">
                <div class="tile tile-orange tile-about-customers">
                    <div class="title">
                        250
                    </div>
                    <div class="text">
                        <div>
                            Довольных клиентов
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 sol-xs-12">
                <div class="tile tile-red tile-about-meters">
                    <div class="title">
                        50000
                    </div>
                    <div class="text">
                        <div>
                            Спланированных квадратных метров
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 h-padding-160 v-padding-50">
            <p class="fs-bigger margin-0">Мы занимаемся разработкой дизайна интерьеров жилых и коммерческих объектов, а также средовым дизайном,
            который включает в себя проектирование фасадов зданий, малых архитектурных форм, выставочных стендов и
            ландшафтный дизайн.</p>
        </div>
    </div>

    <hr class="divider">

    <div class="center">
        <h3 class="backlight">Нам доверяют</h3>
    </div>

    <div class="row our-customers">
    <?php for($i = 1; $i <= 8; $i++): ?>
        <img src="<?php bloginfo('stylesheet_directory') ?>/images/customers/<?php echo $i ?>.jpg" />
    <?php endfor; ?>
    </div>

    <div class="row">
            <?php
            /*
             * Run the loop to output the posts.
             * If you want to overload this in a child theme then include a file
             * called loop-index.php and that will be used instead.
             */
//                get_template_part( 'loop', 'index' );
            ?>
    </div><!-- .row -->

    <div class="row">
        <?php if ( is_active_sidebar( 'first-footer-widget-area' ) ) : ?>
            <div id="first" class="widget-area">
                <?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
            </div><!-- #first .widget-area -->
        <?php endif; ?>
    </div>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>