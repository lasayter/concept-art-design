<?php
    $options = get_option('concept_art_and_design_options');
    $phoneNumber = $options['concept_art_and_design_phone_number'];
?>

        </div>
        <div id="footer" class="container-fluid">
            <div class="container">
                <footer>
                    <div class="row">
                        <div class="col-md-4 copyrights text">
                            © Concept Art & Design <?php echo date('Y') ?>
                        </div>
                        <div class="col-md-4 center text">
                            <span style="vertical-align:middle;">Москва&nbsp;&nbsp;</span>
                            <a href="tel:<?php echo $phoneNumber ?>"><tel style="font-size:1.5em; vertical-align:middle;"><?php echo $phoneNumber ?></tel></a>
                        </div>
                        <div class="col-md-4">

                        </div>
                    </div>
                </footer>
            </div>
        </div>

        <?php wp_footer() ?>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
        <script>

            $(document).ready(function() {

                var docHeight = $(window).height();
                var footerHeight = $('#footer').height();
                var footerTop = $('#footer').position().top + footerHeight;

                if (footerTop < docHeight) {
                    $('#footer').css('margin-top', 0 + (docHeight - footerTop) + 'px');
                }
            });
        </script>
    </body>
</html>